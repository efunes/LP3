/* Listing 5.02 - Augusto Silvero*/

#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <stdio.h>

/* We must define union semun ourselves. */

union semun {
	int val;
    struct semid_ds *buf;
    unsigned short int *array;
    struct seminfo *__buf;
};

/* Obtain a binary semaphore’s ID, allocating if necessary. */

int binary_semaphore_allocation (key_t key, int sem_flags)
{
    return semget (key, 1, sem_flags);
}

/* Deallocate a binary semaphore. All users must have finished their
    use. Returns -1 on failure. */

int binary_semaphore_deallocate (int semid)
{
    union semun ignored_argument;
    return semctl (semid, 1, IPC_RMID, ignored_argument);
}

int main()
{
    key_t Llave;
    int semaforo;

    struct sembuf Procesos;
    Llave = ftok("bin/ls",1);

    printf("Se realizara allocation de un semaforo en bin/ls...\n");
    
    semaforo = binary_semaphore_allocation(Llave, 0600 | IPC_CREAT);
    
    if(semaforo!=-1){
    	printf("Semaforo localizado exitosamente. ID = %d.\n",semaforo);
    }else{
    	printf("Hubo un problema para localizar un semaforo, se cerrara el programa\n");
    	return -1;
    }
    
    printf("Se realizara deallocate del semaforo...\n");
    if(binary_semaphore_deallocate(semaforo)!=-1){
    	printf("Semaforo de ID = %d delocalizado exitosamente.\n",semaforo);
    }else{
    	printf("Hubo un problema para delocalizar el semaforo, se cerrara el programa\n");
    	return -1;
    }
    return 0;
}


