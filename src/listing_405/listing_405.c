/*Listing 4.5 , detached*/ 
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
typedef struct data{char type[20];}data;
void* thread_function (void* thread_arg)
{
	data *dt=(data *)thread_arg;
	printf("El hilo (%s) duerme 5 segundos\n",dt->type);
	sleep(5);
	printf("El hilo (%s) finaliza\n",dt->type);
	return NULL;
}


int main (){

	pthread_t thread1;
	pthread_t thread2;
	data data1;
	data data2;
	
	strcpy(data1.type,"JOINED");
	pthread_attr_t attr;
	pthread_attr_init (&attr);
	pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);
	pthread_create (&thread1,NULL, &thread_function, &data1);
	sleep(3);
	strcpy(data2.type,"DETACHED");
	pthread_create (&thread2,&attr, &thread_function, &data2);
	pthread_join (thread1, NULL);
	pthread_join (thread2, NULL);
	return 0;
}
