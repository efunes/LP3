/*Listing 4.2 , thread-create2*/ 
#include <pthread.h>
#include <stdio.h>

/* Parametros para print_function. */
int stop[256];
struct char_print_parms{
	/* The character to print. */
	char character;
	/* The number of times to print it. */
	int count;
};

/* Prints a number of characters to stderr, as given by PARAMETERS,
	which is a pointer to a struct char_print_parms. */

void* char_print (void* parameters){
	/* Cast the cookie pointer to the right type. */
	struct char_print_parms* p = (struct char_print_parms*) parameters;
	int i;

	for (i = 0; i < p->count; ++i){
		fputc (p->character, stderr);
	}
	stop[p->character]=1;
	return NULL;
}

/* The main program. */

int main ()
{
	pthread_t thread1_id;
	pthread_t thread2_id;
	struct char_print_parms thread1_args;
	struct char_print_parms thread2_args;
    stop['x']=stop['o']=0;
	/* Imprime 'x's. */
	thread1_args.character = 'x';
	thread1_args.count = 300;
	pthread_create (&thread1_id, NULL, &char_print, &thread1_args);

	/* Imprime o's. */
	thread2_args.character = 'o';
	thread2_args.count = 200;
	pthread_create (&thread2_id, NULL, &char_print, &thread2_args);
    while(stop['x']==0 || stop['o']==0){
        /*Espera a que finalizen los hilos, en el siguiente listing 
         *se podra apreciar como se hacen correctamente
         */
    }
    printf("\n");
	return 0;
}
