/* Listing 5.04 - Augusto Silvero */

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>

/* Wait on a binary semaphore. Block until the semaphore value is positive, then
    decrement it by 1. */

int binary_semaphore_wait (int semid)
{
    struct sembuf operations[1];
    /* Use the first (and only) semaphore. */
    operations[0].sem_num = 0;
    /* Decrement by 1. */
    operations[0].sem_op = -1;
    /* Permit undo’ing. */
    operations[0].sem_flg = SEM_UNDO;

    return semop (semid, operations, 1);
}

/* Post to a binary semaphore: increment its value by 1.
    This returns immediately. */

int binary_semaphore_post (int semid)
{
    struct sembuf operations[1];
    /* Use the first (and only) semaphore. */
    operations[0].sem_num = 0;
    /* Increment by 1. */
    operations[0].sem_op = 1;
    /* Permit undo’ing. */
    operations[0].sem_flg = SEM_UNDO;

    return semop (semid, operations, 1);
}

int main()
{
    key_t Llave;
    int semaforo;

    struct sembuf Procesos;
    Llave = ftok("bin/ls",1);

    printf("Se creara un semaforo...\n");

    semaforo = semget(Llave, 1, 0600 | IPC_CREAT);

    if(semaforo!=-1){
    	printf("Semaforo creado exitosamente. ID = %d.\n",semaforo);
    }else{
    	printf("Hubo un problema para crear el semaforo, se cerrara el programa\n");
    	return -1;
    }

    printf("\nSe pondrá el semáforo en espera...\n");

    if(binary_semaphore_wait (semaforo)!=-1){
    	printf("Semaforo en espera...\n");
    }else{
    	printf("Hubo un problema para realizar la operacion, se cerrara el programa\n");
    	return -1;
    }

    printf("\nSe pondrá el semáforo en post...\n");

    if(binary_semaphore_post (semaforo)!=-1){
    	printf("Semaforo en post. Operacion realizada con exito\n");
    }else{
    	printf("Hubo un problema para realizar la operacion, se cerrara el programa\n");
    	return -1;
    }
    return 0;
}
