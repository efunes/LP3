#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

/*3.05 Using a signal handler*/

sig_atomic_t sigusr1_count = 0;

void handler (int signal_number)
{
	++sigusr1_count;
}

int main ()
{
	/*La signal sigusr1 no es controlada por el SO, por lo tanto solo se puede lanzar por el mismo programa con raise*/

	struct sigaction sa;
	memset (&sa, 0, sizeof (sa));
	sa.sa_handler = &handler;
	sigaction (SIGUSR1, &sa, NULL);
	printf("El programa durara 15 segundos, cada segundo se lanzara una signal de tipo SIGUSR1\n");	
	for(int i=0;i<15;i++)
	{
		printf("Se ha lanzado una signal de tipo SIGUSR1 con raise\n");		
		raise(SIGUSR1);
		sleep(1);
	}	
	printf ("SIGUSR1 se ha lanzado %d times\n", sigusr1_count);
	return 0;
}