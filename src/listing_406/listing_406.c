/*Listing 4.6 , critical-section*/ 
#include <pthread.h>
#include <stdio.h>
#include <string.h>

int balances[20];
int tb[20][20];
typedef struct trans{	int from;
			int to;
			int cant;					
			}trans;

/* Transfer DOLLARS from account FROM_ACCT to account TO_ACCT. Return
	0 if the transaction succeeded, or 1 if the balance FROM_ACCT is
	too small. */

void * process_transaction (void *args){
	trans * data=(trans *)args;
	int from=data->from;
	int to=data->to;
	int cant=data->cant;
	int old_cancel_state; 
	if (balances[from] < cant){
		printf("%2d no posee los fondos suficientes para transferir %3d (posee:%3d)\n",from+1,cant,balances[from]);
		return (void *)1;
	}
	/* Begin critical section. */
	pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, &old_cancel_state);
	/* Move the money. */
	printf("%2d(%3d) deposita a %2d(%3d) %3d dolares",from+1,balances[from],to+1,balances[to],cant);
	balances[to] += cant;
	balances[from] -= cant;
	printf(";nuevo balance %2d(%3d) y %2d(%3d)\n",from,balances[from],to,balances[to]);
	/* End critical section. */
	pthread_setcancelstate (old_cancel_state, NULL);

	return 0;
}
int thread_creator(int *f,int *t,int *c,int lvl){
	int r,a0,aa,b0,a1,b1;
	trans data;
	if(lvl==0)return 0;lvl--;

	data.from=f[lvl];
	data.to=t[lvl];
	data.cant=c[lvl];


	pthread_t thread;
	thread_creator(f,t,c,lvl);
	pthread_create (&thread,NULL, &process_transaction, &data);
	pthread_join (thread, (void*) &r);
	
}
int main(){
	int i,j,k;
	int from[20];
	int to[20];
	int cant[20];
	for(i=0;i<10;i++){
		balances[i]=100*(1+i%5);
	}
	for(i=0;i<20;i++){
		from[i]=i%10;
		to[i]=(2*i+i/4)%10;
		if(from[i]==to[i])to[i]=(to[i]+1)%10;
		cant[i]=i*i-i+1;
	}
	thread_creator(from,to,cant,20);
	return 0;
}
