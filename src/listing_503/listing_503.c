/* Listing 5.03 - Augusto Silvero*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>

/* We must define union semun ourselves.*/

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short int *array;
    struct seminfo *__buf;
};

/* Initialize a binary semaphore with a value of 1.*/

int binary_semaphore_initialize (int semid)
{
    union semun argument;
    unsigned short values[1];
    values[0] = 1;
    argument.array = values;
    return semctl (semid, 0, SETALL, argument);
}

int main()
{
    key_t Llave;
    int semaforo;

    struct sembuf Procesos;
    Llave = ftok("bin/ls",1);

    printf("Se creara un semaforo...\n");

    semaforo = semget(Llave, 1, 0600 | IPC_CREAT);

    if(semaforo!=-1){
    	printf("Semaforo creado exitosamente. ID = %d.\n",semaforo);
    }else{
    	printf("Hubo un problema para crear el semaforo, se cerrara el programa\n");
    	return -1;
    }

    printf("Se incializara un semaforo binario con un valor 1...\n");

    if(binary_semaphore_initialize (semaforo)!=-1){
    	printf("Semaforo de ID = %d inicializado exitosamente.\n",semaforo);
    }else{
    	printf("Hubo un problema para inicializar el semaforo, se cerrara el programa\n");
    	return -1;
    }
    return 0;
}
