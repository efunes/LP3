/*Listing 4.10 , job-queue1*/
#include <malloc.h>
#include <stdio.h>
#include <pthread.h>
typedef struct job {
	/* Link field for linked list. */
	struct job* next;
	int tipo;
	int valor;
	/* Other fields describing work to be done... */
}job;

/* A linked list of pending jobs. */
struct job* job_queue;

/* Process queued jobs until the queue is empty. */
int process_job(job * next_job,int s){
	if(next_job->tipo == 0){
		printf("%d+%d=%d\n",s,next_job->valor,s+next_job->valor);
		s=s+next_job->valor;
	}else if(next_job->tipo == 1){
		printf("%d-%d=%d\n",s,next_job->valor,s-next_job->valor);	
		s=s-next_job->valor;
	}else if(next_job->tipo == 2){
		printf("%d*%d=%d\n",s,next_job->valor,s*next_job->valor);	
		s=s*next_job->valor;
	}else if(next_job->tipo == 3 && next_job->valor!=0){
		printf("%d/%d=%d\n",s,next_job->valor,s/next_job->valor);	
		s=s/next_job->valor;
	}else{
		printf("Operacion no soportada\n");
	}
	return s;
}
void* thread_function (void* arg)
{
	int s=((int *)arg)[0];
	while (job_queue != NULL) {
		/* Get the next available job. */
		struct job* next_job = job_queue;
		/* Remove this job from the list. */
		job_queue = job_queue->next;
		/* Carry out the work. */
		s=process_job (next_job,s);
		/* Clean up. */
		free (next_job);
	}
	((int *)arg)[0]=s;
}
void addQueque(int val,int type){
	job * aux=job_queue;
	if(aux==NULL){
		aux=job_queue=(job *)malloc(sizeof(job));
	}else{
		while(aux->next!=NULL){
			aux=aux->next;
		}
		aux->next=(job *)malloc(sizeof(job));
		aux=aux->next;
	}
	aux->next=NULL;
	aux->tipo=type;
	aux->valor=val;
}
int main(){
	int i=2,j;
	pthread_t hilo;
	addQueque(7,0);
	addQueque(2,1);
	addQueque(25,2);
	addQueque(7,3);
	pthread_create(&hilo,NULL,thread_function,(void *)&i);
	pthread_join(hilo,NULL);
	
	printf("Valor final : %d\n",i);
	return 0;
}

