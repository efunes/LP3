/*Listing 4.7 , tsd*/ 
#include <malloc.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
/* The key used to associate a log file pointer with each thread. */
static pthread_key_t thread_log_key;

/* Write MESSAGE to the log file for the current thread. */

void write_to_thread_log (const char* message)
{
	FILE* thread_log = (FILE*) pthread_getspecific (thread_log_key);
	fprintf (thread_log, "%s\n", message);
}

/* Close the log file pointer THREAD_LOG. */

void close_thread_log (void* thread_log)
{
	fclose ((FILE*) thread_log);
}

void* thread_function (void* args)
{
	int *t = (int *)args;
	char thread_log_filename[20];
	FILE* thread_log;
	char s[10];
	char msg[50];
	/* Generate the filename for this thread's log file. */
	sprintf (thread_log_filename, "thread%d.log", (int) pthread_self ());
	printf("Archivo de registro thread%d.log ha sido creado\n", (int) pthread_self ());
	/* Open the log file. */
	thread_log = fopen (thread_log_filename, "w");
	/* Store the file pointer in thread-specific data under thread_log_key. */
	pthread_setspecific (thread_log_key, thread_log);
	s[0]=t[0]+'0';
	s[1]='\0';
	strcpy(msg,"El hilo numero ");
	strcat(msg,s);
	strcat(msg," inicia.");
	write_to_thread_log (msg);
	write_to_thread_log ("El hilo finaliza exitosamente.");
	return NULL;
}

int main ()
{
	int i;
	int number[5];
	pthread_t threads[5];
	
	/* Create a key to associate thread log file pointers in
		thread-specific data. Use close_thread_log to clean up the file
		pointers. */
	pthread_key_create (&thread_log_key, close_thread_log);
	/* Create threads to do the work. */
	for (i = 0; i < 5; ++i){
		number[i]=i+1;
		pthread_create (&(threads[i]), NULL, thread_function, number+i);
				
	}
	/* Wait for all threads to finish. */
	for (i = 0; i < 5; ++i)
		pthread_join (threads[i], NULL);
	printf("Los \"log\" de 5 hilos han sido creados\n");
	return 0;
}
