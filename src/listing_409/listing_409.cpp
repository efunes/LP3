/*Listing 4.9 , cxx-exit*/
/*Usar compilador de c++*/
#include <pthread.h>
#include <stdio.h>
typedef struct Data{int cant;
					char letra;
					}Data;
class ThreadExitException{
	public:
		/* Create an exception-signaling thread exit with RETURN_VALUE. */
		ThreadExitException (void* return_value): thread_return_value_ (return_value){}

		/* Actually exit the thread, using the return value provided in the
		constructor. */
		void* DoThreadExit (){
			printf("\nHilo finalizado a causa de una excepcion\n");
			pthread_exit (thread_return_value_);
		}

	private:
		/* The return value that will be used when exiting the thread. */
		void* thread_return_value_;
};
bool should_exit_thread_immediately (int i){
	return i<=0;
}
void do_some_work (int i,char c){
	while (1) {
		/* Do some useful things here... */
		printf("%c",c);i--;
		if (should_exit_thread_immediately (i))
			throw ThreadExitException (/* thread's return value = */ NULL);
	}
}

void* thread_function (void* args)
{
	
	Data *d=(Data *)args;
	try {
		do_some_work (d->cant,d->letra);
	}
	catch (ThreadExitException ex) {
		ex.DoThreadExit ();
	}
	return NULL;
}
int main(){
	Data dat;
	pthread_t hilo;
	printf("Imprime 10 x's\n");
	dat.cant=10;
	dat.letra='x';
	pthread_create(&hilo,NULL,thread_function,&dat);
	pthread_join(hilo,NULL);

}
