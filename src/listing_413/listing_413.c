/*Listing 4.13 , spin-condvar*/
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
int thread_flag;
long long int process[3];
pthread_mutex_t thread_flag_mutex;

void initialize_flag ()
{
	pthread_mutex_init (&thread_flag_mutex, NULL);
	thread_flag = 0;
}
void do_work(int i){
	printf("x");
	process[i]++;
}
/* Calls printf repeatedly while the thread flag is set; otherwise
	spins. */
void* thread_function (void* thread_arg)
{
   int i = ((int *)thread_arg)[0];
   int flag_is_set;
	while (1) {
		
		/* Protect the flag with a mutex lock. */
		pthread_mutex_lock (&thread_flag_mutex);
		flag_is_set = thread_flag;
		pthread_mutex_unlock (&thread_flag_mutex);

		if (flag_is_set)do_work(i);
		/* Else don't do anything. Just loop again. */
	}
	return NULL;
}

/* Sets the value of the thread flag to FLAG_VALUE. */

void set_thread_flag (int flag_value)
{
	/* Protect the flag with a mutex lock. */
	pthread_mutex_lock (&thread_flag_mutex);
	thread_flag = flag_value;
	pthread_mutex_unlock (&thread_flag_mutex);
}
int main(){
	int i;
	int n[3]={0,1,2};
	pthread_t hilos[3];
	for(i=0;i<3;i++)pthread_create(hilos+i,NULL,thread_function,(void *)&(n[i]));
	set_thread_flag (1);
	sleep(1);
	set_thread_flag (0);
	printf("\n");
	for(i=0;i<3;i++)printf("x's impresas por el hilo %d:%lli\n",i+1,process[i]);
	return 0;
}   

