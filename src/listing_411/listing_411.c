/*Listing 4.11 , job-queue2*/
#include <malloc.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
struct job {
	/* Link field for linked list. */
	struct job* next;
	char print;
	/* Other fields describing work to be done... */
};

/* A linked list of pending jobs. */
struct job* job_queue;
void process_job (struct job * next_job,int inv){
	
	printf("\t\t\tHilo %d procesa: %c\n",inv,next_job->print);
}
/* A mutex protecting job_queue. */
pthread_mutex_t job_queue_mutex = PTHREAD_MUTEX_INITIALIZER;
int z;
/* Process queued jobs until the queue is empty. */

void* thread_function (void* arg)
{
	int i=((int *)arg)[0];
	
	while (1) {
		struct job* next_job;
		/* Lock the mutex on the job queue. */
		pthread_mutex_lock (&job_queue_mutex);
		/* Now it's safe to check if the queue is empty. */
		if (job_queue == NULL)
			next_job = NULL;
		else {
			/* Get the next available job. */
			next_job = job_queue;
			/* Remove this job from the list. */
			job_queue = job_queue->next;
			printf("Hilo %d adquiere: %c\n",i,next_job->print);
		}
		/* Unlock the mutex on the job queue because we're done with the
			queue for now. */
		if(i==1)sleep(1);
		pthread_mutex_unlock (&job_queue_mutex);
		if(i==2)sleep(1);
		/* Was the queue empty? If so, end the thread. */
		if (next_job == NULL)
			break;
		/* Carry out the work. */
		process_job (next_job,i);
		if(i==3)sleep(1);
		/* Clean up. */
		free (next_job);
	}
	return NULL;
}

int main(){
	int i;
	int n[3]={1,2,3};
	struct job *aux;
	pthread_t hilos[6];

	aux=job_queue=(struct job *)malloc(sizeof(struct job));
	for(i='a';i<'z';i++){
		aux->print=(char)i;
		aux->next=(struct job *)malloc(sizeof(struct job));
		aux=aux->next;
	}
	aux->print='z';
	printf("Mutex en la asignacion de tareas\n");
	printf("El hilo 1 duerme 1 segundo durante el mutex\n");
	printf("El hilo 2 duerme 1 segundo despues el mutex y antes del proceso\n");
	printf("El hilo 3 duerme 1 segundo despues del proceso\n");
	for(i=0;i<3;i++)pthread_create(hilos+i,NULL,thread_function,(void *)&(n[i]));
	for(i=0;i<3;i++)pthread_join(hilos[i],NULL);

}
