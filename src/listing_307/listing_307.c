#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>

/*Listing 3.7 Cleaning up Children by Handling SIGCHLD*/

sig_atomic_t child_exit_status;

void clean_up_child_process (int signal_number)
{
	/* Clean up the child process. */
	int status;
	wait (&status);
	/* Store its exit status in a global variable. */
	child_exit_status = status;
}

int main ()
{
	/* Handle SIGCHLD by calling clean_up_child_process. */
	struct sigaction sigchld_action;
	memset (&sigchld_action, 0, sizeof (sigchld_action));
	sigchld_action.sa_handler = &clean_up_child_process;
	sigaction (SIGCHLD, &sigchld_action, NULL);
	
	pid_t child_pid;
	
	printf ("El process ID del programa principal es %d\n", (int) getpid ());

	printf("Se crea al hijo\n");	

	child_pid = fork ();
	
	if (child_pid != 0) /*Si es el proceso padre entonces*/
	{
		printf ("Este es el proceso padre, con ID %d, esperare a mi hijo para finalizar\n", (int) getpid ());
		raise(SIGCHLD);	/*Voy a esperar a que mi hijo concluya para seguir yo*/
		printf("Mi proceso hijo ya ha terminado, status de salida %d\n", child_exit_status); /*status de finalizacion es 0*/
		
	}
	else /*Es el proceso hijo, que obviamente durara mas tiempo que el proceso padre, por lo tanto se usa la signal SIGCHLD para obligar al padre a esperarlo*/
	{	
		printf("Soy el proceso hijo, con ID %d, tardare 3 segundos en acabar\n", (int) getpid ());		
		sleep(3);		
		printf ("El proceso hijo ha finalizado\n");		
		
	}
	return 0;
}