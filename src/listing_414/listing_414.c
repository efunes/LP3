/*Listing 4.14 , condvar*/
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
int thread_flag;
long long int process[3];
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex;
void do_work(int i);
void initialize_flag ()
{
	/* Initialize the mutex and condition variable. */
	pthread_mutex_init (&thread_flag_mutex, NULL);
	pthread_cond_init (&thread_flag_cv, NULL);
	/* Initialize the flag value. */
	thread_flag = 0;
}

/* Calls do_work repeatedly while the thread flag is set; blocks if
	the flag is clear. */

void* thread_function (void* thread_arg)
{
	int i = ((int *)thread_arg)[0];
	/* Loop infinitely. */
	while (1) {
		/* Lock the mutex before accessing the flag value. */
		pthread_mutex_lock (&thread_flag_mutex);
		while (!thread_flag)
			/* The flag is clear. Wait for a signal on the condition
				variable, indicating that the flag value has changed. When the
				signal arrives and this thread unblocks, loop and check the
				flag again. */
			pthread_cond_wait (&thread_flag_cv, &thread_flag_mutex);
		/* When we�ve gotten here, we know the flag must be set. Unlock
			the mutex. */
		do_work (i);
		
		/* Do some work. */
		
	}
	return NULL;
}

/* Sets the value of the thread flag to FLAG_VALUE. */
void do_work(int i){
	printf("x");
	pthread_mutex_unlock (&thread_flag_mutex);
	process[i]++;
}
void set_thread_flag (int flag_value)
{
	/* Lock the mutex before accessing the flag value. */
	pthread_mutex_lock (&thread_flag_mutex);
	/* Set the flag value, and then signal in case thread_function is
		blocked, waiting for the flag to become set. However,
		thread_function can�t actually check the flag until the mutex is
		unlocked. */
	thread_flag = flag_value;
	pthread_cond_signal (&thread_flag_cv);
	/* Unlock the mutex. */
	pthread_mutex_unlock (&thread_flag_mutex);
}
int main(){
	int i;
	int n[3]={0,1,2};
	pthread_t hilos[3];
	for(i=0;i<3;i++)pthread_create(hilos+i,NULL,thread_function,(void *)&(n[i]));
	set_thread_flag (1);
	sleep(1);
	set_thread_flag (0);
	printf("\n");
	for(i=0;i<3;i++)printf("x's impresas por el hilo %d:%lli\n",i+1,process[i]);
	return 0;
}
