/*Listing 4.8 , cleanup*/
#include <malloc.h>
#include <pthread.h>
#include <stdio.h>

typedef struct data{int * vector;
					int longitud;
					int div;
					}Data;

/* Allocate a temporary buffer. */

void* allocate_buffer (size_t size){
	printf("   Se reserva %d bytes para el uso del hilo\n",(int)size);
	return malloc (size);
}

/* Deallocate a temporary buffer. */

void deallocate_buffer (void* buffer){
	printf("   Se libera la memoria utilizada\n");
	free (buffer);
}
void * div_vector(void *args){
	int i;
	Data *arg=(Data *)args;
	size_t tam;
	if(sizeof(int)*arg->longitud>sizeof(int)){
		tam=sizeof(int)*arg->longitud;
	}else{
		tam=sizeof(int);
	}
	printf("Dividir el vector entre %d: \n",arg->div);
	int * temp_buffer =(int *)allocate_buffer (tam);
	
	pthread_cleanup_push (deallocate_buffer, temp_buffer);
	
	if(arg->div==0 || arg->longitud<0){
		printf(" Operacion cancelada ");
		if(arg->div==0)printf("(No se puede dividir entre 0) ");
		if(arg->longitud<0)printf("(La longitud insertada es inválida) ");
		printf("\n");
		pthread_exit(args);
	}
	printf(" Resultado: ");
	for(i=0;i<arg->longitud;i++){
		temp_buffer[i]=arg->vector[i]/arg->div;
		printf("%d ",temp_buffer[i]);
	}
	printf("\n");
	pthread_cleanup_pop (1);
	
	return 0;	
}

int main(){
	int i,j;
	int *res;
	pthread_t hilo;
	Data vector;
	vector.vector=(int *)malloc(3*sizeof(int));
	vector.vector[0]=6;
	vector.vector[1]=12;
	vector.vector[2]=18;
	vector.longitud=3;
	vector.div=2;
	printf("Vector de referencia:");
	for(i=0;i<3;i++)printf("%d ",vector.vector[i]);printf("\n");
	pthread_create(&hilo,0,div_vector,&vector);
	pthread_join(hilo,0);
	vector.div=0;
	pthread_create(&hilo,0,div_vector,&vector);
	pthread_join(hilo,0);
	vector.div=3;
	pthread_create(&hilo,0,div_vector,&vector);
	pthread_join(hilo,0);
	return 0;
}
