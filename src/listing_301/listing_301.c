

/*Listing 3.1 Printing the PID*/ 
#include <stdio.h> 
#include <unistd.h> 
int main () 
{
    printf ("El process ID es %d\n", (int) getpid ());
    printf ("El process ID padre es %d\n", (int) getppid ());
    return 0; 
}

