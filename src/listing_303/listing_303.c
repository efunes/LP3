/*3.03 Using fork to Duplicate a Program’s Process*/

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main ()
{
	pid_t child_pid;

	printf ("El process ID del programa principal es %d\n", (int) getpid ());

	child_pid = fork ();
	
	printf("Se usa fork para duplicar el proceso del programa\n");	
	
	if (child_pid != 0) {
		printf ("Este es el proceso padre, con ID %d\n", (int) getpid ());
		printf ("El process ID del proceso hijo es %d\n", (int) child_pid);
	}
	else
		printf ("Este es el proceso hijo, con ID %d\n", (int) getpid ());
	return 0;
} 