/*Listing 4.1 , thread-create*/ 
#include <pthread.h> 
#include <stdio.h> 
int xs,os,limit; 
/*Funcion que ejecutara el Hilo*/ 
void* print_xs (void* unused){
 	while (xs+os<limit){
 	 		fputc ('x', stderr);
 	 		xs++;
 	}
 	return NULL;
} 
/* The main program.*/ 
int main (){
 	limit = 100000;
 	pthread_t thread_id;
 	pthread_create (&thread_id, NULL, &print_xs, NULL);
 	/*Mientras el hilo imprime x's, el main imprime o's*/
 	while (xs+os<limit){
 	 		fputc ('o', stderr);
 	 		os++;
    }
    printf("\nCantidad de \'o\' impresas: %d",os);
    printf("\nCantidad de \'x\' impresas: %d\n",xs);
    return 0;
}


