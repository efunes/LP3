#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h> 

/*3.06 Making a zombie process*/

int main () 	
{
	pid_t child_pid;

 	/* Create a child process. */
	printf("Se crea al proceso hijo\n"); 	
	child_pid = fork ();
 	if (child_pid > 0) 
	{ 		
		/* This is the parent process. Sleep for a minute. */
		printf("Este es el proceso padre, se congelara durante un minuto\n"); 		
		sleep (60);
 	} 	
	else {
 		/* This is the child process. Exit immediately. */
		printf("Este es el proceso hijo, se finalizara inmediatamente\n");		
		exit (0);
 	}
	printf("El programa ha finalizado\n"); 	
	return 0;
}
